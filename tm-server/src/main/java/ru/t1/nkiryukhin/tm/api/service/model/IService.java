package ru.t1.nkiryukhin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.model.IRepository;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> {

    @Nullable
    M add(@Nullable M model);

    void removeOne(@Nullable M model);

    void update(@NotNull M model);

}