package ru.t1.nkiryukhin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @Nullable
    List<UserDTO> findAll();

    int getSize();

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    UserDTO findOneById(@NotNull String id);

}
