package ru.t1.nkiryukhin.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ApplicationVersionRequest extends AbstractRequest {
}